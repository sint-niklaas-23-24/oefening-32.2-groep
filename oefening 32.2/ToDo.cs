﻿using System;
using System.ComponentModel;

namespace oefening_32._2
{
    internal class ToDo : INotifyPropertyChanged, IDataErrorInfo
    {
        private string _wanneer;
        private string _wat;
        private string _wie;

        public ToDo() { }
        public ToDo(string wie, string wat, string wanneer)
        {
            Wanneer = wanneer;
            Wie = wie;
            Wat = wat;
        }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                if (columnName == "Wanneer")
                {
                    if (string.IsNullOrEmpty(Wanneer))
                        result = "Geef een datum in.";
                }
                if (columnName == "Wie")
                {
                    if (string.IsNullOrEmpty(Wie))
                        result = "Geef een naam in.";
                }
                if (columnName == "Wat")
                {
                    if (string.IsNullOrEmpty(Wat))
                        result = "Geef een activiteit in.";
                }
                return result;
            }
        }

        public string Wanneer { get { return _wanneer; } set { _wanneer = value; } }
        public string Wat { get { return _wat; } set { _wat = value; } }
        public string Wie { get { return _wie; } set { _wie = value; } }

        public string Error => throw new NotImplementedException();

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return Wanneer + " " + Wie + ": " + Wat;
        }
    }
}
