﻿using System.Collections.Generic;
using System.Windows;

namespace oefening_32._2
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<ToDo> lstToDo;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lstToDo = new List<ToDo>();
            ToDo todo1 = new ToDo("Bernd", "Karaoke", "22/02/2024");
            ToDo todo2 = new ToDo("Bart", "Voetbal", "11/01/2022");
            ToDo todo3 = new ToDo("Yorben", "WoW", "09/09/1999");

            lstToDo.Add(todo1);
            lstToDo.Add(todo2);
            lstToDo.Add(todo3);

            cmbToDo.ItemsSource = lstToDo;
            cmbToDo.SelectedIndex = 0;
        }
    }
}
